package com.vti.springframeworkra59.service.demo;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class BeanDemo {
    private String key;
    private String value;

}
