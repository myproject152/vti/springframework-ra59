package com.vti.springframeworkra59.service;

import com.vti.springframeworkra59.modal.dto.AccountRequest;
import com.vti.springframeworkra59.modal.entity.Account;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
@Service
public interface IAccountService {
    Page<Account> getAllAccount(AccountRequest request);

    Account getById(int id);

    Account update(int id, Account account);

    void create(Account account);

    void delete(int id);

    List<Account> searchByName(String name, String email); // name = abc  // List Account có chứa abc || Account có tên đầy đủ là abc
}
