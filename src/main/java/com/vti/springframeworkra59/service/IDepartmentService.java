package com.vti.springframeworkra59.service;

import com.vti.springframeworkra59.modal.dto.DepartmentRequest;
import com.vti.springframeworkra59.modal.entity.Department;

import java.util.List;

public interface IDepartmentService {
    List<Department> getAll();

    List<Department> search(DepartmentRequest request);

    Department getById(int id);

    void delete(int id);
}
