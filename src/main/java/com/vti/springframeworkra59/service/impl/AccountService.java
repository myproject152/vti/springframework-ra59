package com.vti.springframeworkra59.service.impl;

import com.vti.springframeworkra59.modal.dto.AccountRequest;
import com.vti.springframeworkra59.modal.entity.Account;
//import com.vti.springframeworkra59.repository.AccountRepository;
import com.vti.springframeworkra59.repository.AccountRepository;
import com.vti.springframeworkra59.service.IAccountService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Service
public class AccountService implements IAccountService {
    @Autowired
    private AccountRepository repository;

    @Override
    public Page<Account> getAllAccount(AccountRequest request) {
        Pageable pageable = Pageable.ofSize(request.getPageSize()).withPage(request.getPageNumber());

        PageRequest pageRequest;
        if ("DESC".equals(request.getSortType())){
            pageRequest = PageRequest.of(request.getPageNumber(), request.getPageSize(), Sort.by(request.getSortBy()).descending());
        } else {
            pageRequest = PageRequest.of(request.getPageNumber(), request.getPageSize(), Sort.by(request.getSortBy()).ascending());
        }
        return repository.findAllByUsernameContains(request.getName(), pageRequest);
//        return null;
    }

    @Override
    public Account getById(int id) {
        // Optional là 1 dạng đối tượng chống null...
        Optional<Account> accountOptional = repository.findById(id);
        if (accountOptional.isEmpty()){
            return null;
        } else {
            return accountOptional.get();
        }
    }

    @Override
    public Account update(int id, Account account) {
        Account accountDb = getById(id);
        if (accountDb != null){
            return repository.save(account);
        }
        return null;
    }

    @Override
    public void create(Account account) {
        repository.save(account);
    }

    @Override
    public void delete(int id) {
        if (repository.findById(id).isEmpty()){
            throw new RuntimeException("Account ko tồn tại");
        }
        repository.deleteById(id);
    }

    @Override
    public List<Account> searchByName(String name, String email) {
        return repository.findAllByUsernameContainsAndEmailContains(name, email);
    }
}
