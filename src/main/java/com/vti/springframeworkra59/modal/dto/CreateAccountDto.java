package com.vti.springframeworkra59.modal.dto;

import lombok.Data;

@Data
public class CreateAccountDto {
    private String username;

    private String email;

    private String password;



}
