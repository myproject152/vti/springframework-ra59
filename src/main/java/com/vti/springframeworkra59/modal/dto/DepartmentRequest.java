package com.vti.springframeworkra59.modal.dto;

import com.vti.springframeworkra59.modal.entity.TypeDepartment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentRequest {
    private String name;
    private int minTotalMember;
    private int maxTotalMember;
    private TypeDepartment typeDepartment;
}
