package com.vti.springframeworkra59.modal.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "`Account`")
@Getter
@Setter
public class Account implements Serializable {
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "`username`", length = 50, unique = true)
    private String username;

    @Column(name = "`email`", length = 50, nullable = false)
    private String email;

    @Column(name = "`password`", length = 50, nullable = false)
    private String password;

}
