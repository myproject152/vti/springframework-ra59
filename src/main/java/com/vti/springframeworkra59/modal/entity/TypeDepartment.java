package com.vti.springframeworkra59.modal.entity;

public enum TypeDepartment {
    Dev, Test, ScrumMaster, PM
}
