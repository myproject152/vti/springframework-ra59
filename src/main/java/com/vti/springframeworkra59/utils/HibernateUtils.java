package com.vti.springframeworkra59.utils;

import com.vti.springframeworkra59.modal.entity.Account;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtils {
    private SessionFactory sessionFactory;

    public SessionFactory buildSessionFactory() {
        if (sessionFactory == null){
            Configuration configuration = configure();
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
            return configuration.buildSessionFactory(serviceRegistry);
        }
        return sessionFactory;
    }
// no SQL: Resdis, Monggo DB....
    // SQL: My SQL, Oracle, Postgres....
    private Configuration configure(){
        // load configuration
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        // add entity
        configuration.addAnnotatedClass(Account.class);
        return configuration;
    }

    public void closeFactory(){
        if (!sessionFactory.isClosed()){
            sessionFactory.close();
        }
    }

    public Session openSession(){
        return buildSessionFactory().openSession();
    }
}
