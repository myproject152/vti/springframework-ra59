package com.vti.springframeworkra59;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringframeworkRa59Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringframeworkRa59Application.class, args);
    }

}
