package com.vti.springframeworkra59.repository;

import com.vti.springframeworkra59.modal.entity.Department;
import com.vti.springframeworkra59.modal.entity.TypeDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer>, JpaSpecificationExecutor<Department> {
    // Search Department theo tên method
    List<Department> findAllByNameContainingAndTotalMemberBetweenAndTypeEquals(String name, int minTotalMember, int maxTotalMember, TypeDepartment type);

    // Cách 2.1: Seach Dp theo câu lệnh HQL
    @Query(value = "SELECT dp FROM Department dp " +
            "WHERE dp.name like :name AND dp.totalMember BETWEEN :minTotalMember AND :maxTotalMember AND dp.type = :type")
    List<Department> searchV1(String name, int minTotalMember, int maxTotalMember, TypeDepartment type);

    // Cách 2.2: Seach Dp theo câu lệnh SQL (nativeQuery = true)
    @Query(value = "SELECT * FROM Department dp " +
            "WHERE dp.name like :name AND dp.total_member BETWEEN :minTotalMember AND :maxTotalMember AND dp.type = :type", nativeQuery = true)
    List<Department> searchV2(String name, int minTotalMember, int maxTotalMember, String type);
}
