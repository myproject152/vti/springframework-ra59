package com.vti.springframeworkra59.repository;

import com.vti.springframeworkra59.modal.entity.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {
    List<Account> findAllByUsernameContainsAndEmailContains(String username, String email);

    Page<Account> findAllByUsernameContains(String username, Pageable pageable);


    Optional<Account> findFirstByUsername(String username);
}
