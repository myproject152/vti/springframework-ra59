package com.vti.springframeworkra59.controller;

import com.vti.springframeworkra59.modal.dto.DepartmentRequest;
import com.vti.springframeworkra59.modal.entity.Department;
import com.vti.springframeworkra59.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/department")
public class DepartmentController {
    private final IDepartmentService service;

    @Autowired
    public DepartmentController(IDepartmentService service) {
        this.service = service;
    }

    @GetMapping("/get-all")
    public List<Department> getAll(){
        return service.getAll();
    }

    @PostMapping("/search")
    public List<Department> search(@RequestBody DepartmentRequest request){
        return service.search(request);
    }

    @GetMapping("/{id}")
    public Department getById(@PathVariable int id){
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable int id){
        try {
            service.delete(id);
            return "Đã xoá thành công!";
        } catch (Exception e){
            return e.getMessage();
        }
    }
}


