package com.vti.springframeworkra59.controller;

import com.vti.springframeworkra59.modal.dto.AccountRequest;
import com.vti.springframeworkra59.modal.dto.CreateAccountDto;
import com.vti.springframeworkra59.modal.entity.Account;
import com.vti.springframeworkra59.service.impl.AccountService;
import com.vti.springframeworkra59.service.demo.BeanDemo;
import com.vti.springframeworkra59.service.demo.Demo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/account")
@CrossOrigin("*")
public class AccountController {
    @Autowired
    private Demo demoV1;

    @Autowired
    @Qualifier(value = "creatBean")
    private BeanDemo beanDemo;

    private final AccountService service;
    @Autowired
    public AccountController(AccountService service) {
        this.service = service;
    }

    @GetMapping("/search")
    public List<Account> getAllAccount(@RequestParam  String username, @RequestParam String email){
        System.out.println(beanDemo);
        return service.searchByName(username, email);
    }

    @PostMapping("/search")
    public Page<Account> all(@RequestBody AccountRequest request){
        return service.getAllAccount(request);
    }

    @GetMapping("/{id}")
    public Account getByIdV1(@PathVariable(name = "id") int id){
        return service.getById(id);
    }

    @GetMapping("/get")
    public Account getByIdV2(@RequestParam int id){
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable(name = "id") int id){
        try {
            service.delete(id);
            return "Xoá thành công";
        } catch (Exception e){
            return e.getMessage();
        }
    }

    @PostMapping("/create")
    public String create(@RequestBody CreateAccountDto accountDto){
        Account account = new Account();
        // Convert từ DTO sang entity

//        account.setEmail(accountDto.getEmail());
//        account.setUsername(accountDto.getUsername());
//        account.setPassword(accountDto.getPassword());
        // C1: Dùng BeansUtils
        BeanUtils.copyProperties(accountDto, account);

        // C2: Dùng thư viện: ModalMapping
        try {
            service.create(account);
            return "thêm mới thành công";
        } catch (Exception e){
            return e.getMessage();
        }
    }

    @PutMapping("/update/{id}")
    public String update(@PathVariable int id,@RequestBody CreateAccountDto accountDto){
        Account account = new Account();
        // Convert từ DTO sang entity
        // C1: Dùng BeansUtils
        BeanUtils.copyProperties(accountDto, account);
        account.setId(id);
        // C2: Dùng thư viện: ModalMapping
        try {
            service.update(id, account);
            return "update thành công";
        } catch (Exception e){
            return e.getMessage();
        }
    }
}
